<?php

/**
 * Clase HomeController para carga el home del proyecto
 */
require 'models/Login.php';
require 'models/Convenio.php';
class HomeController
{
	private $convenio;
	public function __construct()
	{
		$this->login    = new Login;
		$this->convenio = new Convenio;
	}
	public function index()
	{
		if (isset($_SESSION['user'])) {

			require 'views/layout.php';
			$person   = $this->login->getperson();
			$convenio = $this->convenio->getConvenioUser();
			require 'views/profile.php';
			
		} else {
			header('Location: ?controller=login');
		}

	}
}
