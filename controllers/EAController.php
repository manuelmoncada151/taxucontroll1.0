<?php
	
	/**
	 * Clase UserController
	 */

	require 'models/Ea.php';
	require 'models/Convenio.php';



	class EaController
	{
		private $model;
		private $convenio;
	

		
		public function __construct()
		{
			$this->model = new Ea;
			$this->convenio = new Convenio;
		
		}

		public function index() 
		{
	
			require 'views/layout.php';
			//Llamado al metodo que trae todos los usuarios
			$eas = $this->model->getAll();
			require 'views/ea/list.php';
		
		
		}

		//muestra la vista de crear
		public function add() 
		{
			
			
			require 'views/layout.php';
			$convenios = $this->convenio->getAll();
		
			require 'views/ea/new.php';
			
		}
			
		

		// Realiza el proceso de guardar
		public function save()
		{
			$this->model->newEa($_REQUEST);
			$convenios = $this->convenio->getAll();
						
			header('Location: ?controller=ea');
		}
		public function saves()
		{
			$confirm=$this->model->newEa($_REQUEST);
			$convenios = $this->convenio->getAll();
			if($confirm===true){
				header('Location: ?controller=payment');
			}else{
				echo "paila";
				die();
			}		
		}

		//muestra la vista de editar
		public function edit()
		{
			if(isset($_REQUEST['id'])) {
				
				$ID_ENTREAHORRO= $_REQUEST['id'];
				$data = $this->model->getEaById($ID_ENTREAHORRO);
				$convenios = $this->convenio->getAll();
				

				require 'views/layout.php';
				require 'views/ea/edit.php';
				 
			} else {
				echo "Error";
			}
		}

		// Realiza el proceso de actualizar
		public function update()
		{
			if(isset($_POST)) {
				$this->model->editEa($_POST);			
				header('Location: ?controller=ea');				
			} else {
				echo "Error";
			}
		}

		// Realiza el proceso de borrar
		public function delete()
		{			
			$this->model->deleteEa($_REQUEST);		
			header('Location: ?controller=ea');
		}

		
	}