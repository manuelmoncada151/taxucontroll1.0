<?php
	
	/**
	 * Clase UserController
	 */

	require 'models/User.php';
	require 'models/Status.php';
	require 'models/Rol.php';
	require 'models/Login.php';

	class UserController
	{
		private $model;
		private $status;
		private $person;
		private $brand;
		private $rol;

		
		public function __construct()
		{
			$this->model = new User;
			$this->status = new Status;
			$this->rol = new Rol;

		}

		public function index() 
		{
	
			require 'views/layout.php';
			//Llamado al metodo que trae todos los usuarios
			$lstUsers= $this->model->getAll();
			require 'views/user/list.php';
		
		
		}

		//muestra la vista de crear
		public function add() 
		{
			
			
			require 'views/layout.php';
			$rols = $this->rol->getAll();
			require 'views/user/new.php';
			
		}
		public function changepass() 
		{	
			$lastpassI=$_POST['lastpass'];
			$newpass=$_POST['newpass'];
			$user= $_SESSION['user']->ID_USUARIO;
			$lastpass= $this->model->getUsuPass($user);
			if($lastpassI==$lastpass){
				$this->model->newpass($user,$newpass);
				?>
				<script type="text/javascript">
					alert("Contraseña actualizada con exito")
					window.location.href='?controller=home';
				</script>
			<?php
			}else{?>
				<script type="text/javascript">
					alert("La contraseña digitada no coincide con la registrada en nuestro sistema")
					window.location.href='?controller=home';
				</script>
				<?php
			}
		}

		// Realiza el proceso de guardar
		public function save()
		{
			$this->model->newUser($_REQUEST);
						
			header('Location: ?controller=User');
		}

		//muestra la vista de editar
		public function edit()
		{
			if(isset($_REQUEST['id'])) {
				
				$ID_USUARIO= $_REQUEST['id'];
				$data = $this->model->getUserDataById($ID_USUARIO);

				$rols = $this->rol->getAll();

				if( $data[0]->ID_ROL_FK == "2" ){
					$lstConductoresAsignados = $this->model->getAssignedDriversByUserId($ID_USUARIO);
				}
				require 'views/layout.php';
				require 'views/user/edit.php';
				 
			} else {
				echo "Error";
			}
		}

		// Realiza el proceso de actualizar
		public function update()
		{
			if(isset($_REQUEST)) {
				$this->model->editUserPerson($_REQUEST);			
				header('Location: ?controller=User');				
			} else {
				echo "Error";
			}
		}

		// Realiza el proceso de borrar
		public function delete()
		{			
			$this->model->deleteUser($_REQUEST);		
			header('Location: ?controller=User');
		}

		public function updateStatus()
		{
			$user = $this->model->getUserById($_REQUEST['id']);
			$data = [];
			if($user[0]->ID_ESTADO_FK == 1){
			$data =['ID_USUARIO'=> $user[0]->ID_USUARIO,'ID_ESTADO_FK' =>2];
				} elseif($user[0]->ID_ESTADO_FK== 2) {
					$data = ['ID_USUARIO'=> $user[0]->ID_USUARIO,'ID_ESTADO_FK' => 1];

					}
					$this->model->editUser($data);
					header('Location: ?controller=User');
				
			}
	}