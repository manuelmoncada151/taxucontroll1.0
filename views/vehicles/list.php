<!--Apertura container-->
<?php $rol = $_SESSION['user']->ID_ROL_FK;?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title">Dashboard Taxucontroll
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
						href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Vehículos</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-topline-red">
					<div class="card-head">
						<header>Listado de Vehículos</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
							<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body ">
						<div class="row p-b-20">
							<div class="col-md-6 col-sm-6 col-6">
								<div class="btn-group">
									<a href="?controller=veh&method=add" id="addRow1" class="btn btn-info">
										Nuevo <i class="fa fa-plus"></i>
									</a>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-6">
								<div class="btn-group pull-right">
									<button class="btn deepPink-bgcolor  btn-outline dropdown-toggle"
									data-toggle="dropdown">Tools
									<i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;">
											<i class="fa fa-print"></i> Print </a>
										</li>
										<li>
											<a href="javascript:;">
												<i class="fa fa-file-pdf-o"></i> Save as PDF </a>
											</li>
											<li>
												<a href="javascript:;">
													<i class="fa fa-file-excel-o"></i> Export to Excel </a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="table-wrap">
									<div class="table-responsive tblDriverDetail">
										<table class="table table-striped table-hover" id="example" style="margin-top:50px;">
											<thead>
												<tr>
													<th>Placa</th>
													
													<th>Modelo</th>
													<th>Fecha de ingreso</th>
													<th>Marca</th>
													<th>Persona</th>
													<th>Estado</th>
													<th>Acciones</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($vehs as $veh) : ?>
													<tr>
														<td><?php echo $veh->PLACA_VEHICULO ?></td>
														
														<td><?php echo $veh->MODELO_VEHICLO ?></td>
														<td><?php echo $veh->FECHAING_VEHICULO ?></td>
														<td><?php echo $veh->NOM_MARCA ?></td>

														<td><?php echo $veh->NOM_PERSONA ?></td>

														<td>
															<?php
															if($veh->ID_ESTADO_FK ==1)
															{
																?>
																<a href="?controller=veh&method=updateStatus&id=<?php echo $veh->ID_VEHICULO ?>" class="label label-sm box-shadow-1 label-success">Activo</a>
														
																	<?php
																} else {
																	?>
																	<a href="?controller=veh&method=updateStatus&id=<?php echo $veh->ID_VEHICULO?>" class="label label-sm box-shadow-1 label-danger">Inactivo</a>
																	<?php
																}
																?>
															</a>
														</td>
														<td>
															<?php if ($rol == 1) {?>
															<?php
															if($veh->ID_ESTADO_FK ==1)
															{
																?>
															<a href="?controller=veh&method=edit&id=<?php echo $veh->ID_VEHICULO ?>" class="btn btn-tbl-delete btn-xs"><i class="fa fa-edit "></i></a>
																<a href="?controller=veh&method=delete&ID_VEHICULO=<?php echo $veh->ID_VEHICULO?>" class="btn btn-tbl-eliminar btn-xs"><i class="fa fa-trash-o "></i></a>
																	<?php
																}
																?>
															<?php }?>

														</td>
													</tr>
												<?php endforeach ?> 
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>