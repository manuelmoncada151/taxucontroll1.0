<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title">Dashboard Taxucontroll
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
						href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Nueva Novedad</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-box">
					<div class="card-head">
						<header>Nueva Novedad</header>
						<button id="panel-button3"
						class="mdl-button mdl-js-button mdl-button--icon pull-right"
						data-upgraded=",MaterialButton">
						<i class="fa fa-ellipsis-v"></i>
					</button>
					<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"data-mdl-for="panel-button3">
						<li>
							<a href="javascript:;"><i class="fa fa-print"></i> Print </a>
						</li>
						<li>
							<a href="javascript:;"><i class="fa fa-file-pdf-o"></i> Save as PDF </a>
						</li>
						<li>
							<a href="javascript:;"><i class="fa fa-file-excel-o"></i> Export to Excel </a>
						</li>
					</ul>
				</div>
				<div class="card-body " id="bar-parent2">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<!-- text input -->
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<label class="mdl-textfield__label"><label id="oblig">* </label> Fecha</label>
								<input type="date" id="FECH_NOVEDAD" class="mdl-textfield__input" value="<?php echo date("Y-m-d");?>" disabled >
							</div>
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<label class="mdl-textfield__label"><label id="oblig">* </label> Valor</label>
								<input type="text" id="VALOR_NOVEDAD" class="mdl-textfield__input" placeholder="Ingrese Valor" onkeypress="return soloNumeros(event)">
							</div>
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<label class="mdl-textfield__label"><label id="oblig">* </label> Persona</label>
								<select disabled id="ID_PERSONA_FK" class="mdl-textfield__input">
									<option value=""></option>
									<?php
									foreach($persons as $person){
										if ($person->ID_PERSONA == $noveltys[0]->ID_PERSONA_FK) {
											?>
											<option selected value="<?php echo$person->ID_PERSONA ?>"><?php echo $person->nom ?></option>

											<?php
										}else{
											?><option  value="<?php echo$person->ID_PERSONA ?>"><?php echo $person->nom ?></option>
											<?php
										}
									}
									?>
								</select>
							</div>

							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width" >
								<label class="mdl-textfield__label"><label id="oblig">* </label> Tipo</label>
								<select id="tipos" class="mdl-textfield__input">
								</select>
								<div class="col-md-4 mt-4">
									<a href="#" id="addElement" class="btn btn-secondary">+</a>
								</div>
							</div>
							<?php
							if (isset($Types)) {
								?>
								<script>
									var arrTypes = <?php echo json_encode ($Types); ?>
								</script>
								<?php
							}else{
								?>
								<script>
									var arrTypes = []
								</script>
								<?php
							}
							?>
							<div class="form-group" id="list-types">

							</div>
							<!-- checkbox -->

							<!-- radio -->

							<!-- select -->

						</div>
						<div class="col-md-6 col-sm-6">
							<!-- textarea -->
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<label class="mdl-textfield__label"><label id="oblig">* </label> Descripción</label>
								<textarea style="min-height: 113px;" id="DESCRIP_NOVEDAD"class="mdl-textfield__input" placeholder="Ingrese breve descripción de la novedad" required rows="3"
								placeholder="Enter ..."></textarea>
							</div>
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<label class="mdl-textfield__label"><label id="oblig">* </label> Clasificación</label>
								<select id="ID_CLASINOV_FK" class="mdl-textfield__input">
								</select>
							</div>
							<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<label class="mdl-textfield__label"><label id="oblig">* </label> Soporte</label>
								<input type="file" id="URL_SUPNOV" class="mdl-textfield__input" placeholder="Ingrese soporte" required>
							</div>



					<!-- <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
						<form id="id_dropzone" class="dropzone">
							<div class="dz-message">
								<div class="dropIcon">
									<i class="fa fa-eye"></i>
								</div>
								<h3>Click para subir soporte</h3>
								<em>(Recuerde que debe ser un archivo .PDF o una imagen.)
								</em>
							</div>
						</form>
					</div> -->
					<div class="col-lg-12 p-t-20">
						
					</div>

				</div>
			</div>
			<div class="form-group btn-group pull-right">
				<button class="btn btn-primary " id="submit">Guardar</button>
			</div>
		</div>
	</div>
</div>
</div>

<div class="col-lg-3 ">
	<div class="card card-box">
		<div class="card-head">
			<header style="font-size: 15px;">Inactivar Vehículo</header>
			<div class="tools">
				<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
				<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>

			</div>
		</div>
		<div class="card-body no-padding height-9">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
				<input type="text" class="mdl-textfield__input" style="text-align: center;" disabled id="veh1" value="<?php echo$vehs[0]->veh ?>"></input>
			</div>

			<label><input style="margin-left: 50px;" type="checkbox" id="Activo" value="first_checkbox"> Inactivar</label><br>
		</div>
	</div>
</div>


<script src="assets/js/novelty.js"></script>
