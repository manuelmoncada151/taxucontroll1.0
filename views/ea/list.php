<!--Apertura container-->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title">Dashboard Taxucontroll
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
						href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">E. Ahorro</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-topline-red">
					<div class="card-head">
						<header>Listado de Entregas de Ahorro</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
							<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body ">
						<div class="row p-b-20">
							<div class="col-md-6 col-sm-6 col-6">
								<div class="btn-group">
									<!--<a href="?controller=ea&method=add" id="addRow1" class="btn btn-info">
										Nuevo <i class="fa fa-plus"></i>
									</a>-->
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-6">
								<div class="btn-group pull-right">
									<button class="btn deepPink-bgcolor  btn-outline dropdown-toggle"
									data-toggle="dropdown">Opciones
									<i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;">
											<i class="fa fa-print"></i> Print </a>
										</li>
										<li>
											<a href="javascript:;">
												<i class="fa fa-file-pdf-o"></i> Save as PDF </a>
											</li>
											<li>
												<a href="javascript:;">
													<i class="fa fa-file-excel-o"></i> Export to Excel </a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="table-wrap">
									<div class="table-responsive tblDriverDetail">
										<table class="table table-striped table-hover" id="example" style="margin-top:50px;">
											<thead>
												<tr>
													<th>#</th>
													<th>Valor</th>
													<th>Fecha de inicio</th>
													<th>Fecha Fin</th>
													<th>Convenio</th>
													<!--<th>Acciones</th>-->
												</tr>
											</thead>
											<tbody>
												<?php foreach ($eas as $ea) : ?>
													<tr>
														<td><?php echo $ea->ID_ENTREAHORRO ?></td>
														<td><?php echo $ea->VALOR_ENTREAHORRO ?></td>
														<td><?php echo $ea->FECHAINICIO_CONVENIO ?></td>
														<td><?php echo $ea->FECHAFIN_CONVENIO ?></td>

														<td><?php echo $ea->MEDIDA_CONVENIO ?></td>

														<!--<td>

															<a href="?controller=ea&method=edit&id=<?php echo $ea->ID_ENTREAHORRO ?>" class="btn btn-warning">Editar</a>
															<a href="?controller=ea&method=delete&ID_ENTREAHORRO=<?php echo $ea->ID_ENTREAHORRO?>" class="btn btn-danger">Eliminar</a>

														</a>
													</td>-->
												</tr>
											<?php endforeach ?> 
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>