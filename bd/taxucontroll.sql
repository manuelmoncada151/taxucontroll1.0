-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-09-2020 a las 16:55:33
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `taxucontroll`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clasificacion_novedad`
--
CREATE DATABASE taxucontroll;

use taxucontroll;

CREATE TABLE `clasificacion_novedad` (
  `ID_CLASINOV` bigint(20) NOT NULL,
  `NOM_CLASINOV` varchar(25) NOT NULL,
  `DESCRIP_CLASINOV` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clasificacion_novedad`
--

INSERT INTO `clasificacion_novedad` (`ID_CLASINOV`, `NOM_CLASINOV`, `DESCRIP_CLASINOV`) VALUES
(1, 'Mantenimiento ', 'En caso de problemas con el vehiculo'),
(2, 'Incidente', 'En caso de la perdida de una parte del carro'),
(3, 'Accidente', 'Inconveniente con una persona o vehículo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `convenio_producido`
--

CREATE TABLE `convenio_producido` (
  `ID_CONVENIO` bigint(20) NOT NULL,
  `FECHAINICIO_CONVENIO` date NOT NULL,
  `FECHAFIN_CONVENIO` date NOT NULL,
  `MEDIDA_CONVENIO` varchar(20) NOT NULL,
  `VALOR_CONVENIO` decimal(9,2) NOT NULL,
  `AHORRO_PRODUCIDO` decimal(9,2) NOT NULL,
  `S_N_CANCELADO` tinyint(1) NOT NULL,
  `PLACA_VEHICULO_FK` varchar(7) NOT NULL,
  `ID_PERSONA_FK` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `convenio_producido`
--

INSERT INTO `convenio_producido` (`ID_CONVENIO`, `FECHAINICIO_CONVENIO`, `FECHAFIN_CONVENIO`, `MEDIDA_CONVENIO`, `VALOR_CONVENIO`, `AHORRO_PRODUCIDO`, `S_N_CANCELADO`, `PLACA_VEHICULO_FK`, `ID_PERSONA_FK`) VALUES
(1, '2019-09-02', '2024-04-03', 'Semanal', '90000.00', '5000.00', 1, '666-BBB', 1),
(3, '2020-01-01', '2024-04-03', 'Mensual', '80000.00', '10000.00', 1, 'GOP-562', 5),
(4, '2020-01-02', '2024-04-04', 'Mensual', '85000.00', '15000.00', 1, 'TOP-662', 7),
(5, '2020-01-03', '2024-04-05', 'Mensual', '95000.00', '20000.00', 1, 'ZMP-204', 1),
(6, '2020-01-04', '2024-04-06', 'Mensual', '94000.00', '12000.00', 1, 'HJY-789', 5),
(7, '2020-01-05', '2024-04-07', 'Mensual', '98000.00', '13000.00', 1, '999-BBB', 7),
(9, '2020-01-07', '2024-04-09', 'Mensual', '95000.00', '19000.00', 1, 'HMP-710', 5),
(10, '2020-01-08', '2024-04-10', 'Mensual', '95000.00', '21000.00', 1, 'MUI-890', 7),
(13, '2020-01-01', '2024-04-03', 'Mensual', '80000.00', '10000.00', 1, 'GOP-562', 5),
(14, '2020-01-02', '2024-04-04', 'Mensual', '85000.00', '15000.00', 1, 'TOP-662', 7),
(16, '2020-01-04', '2024-04-06', 'Mensual', '94000.00', '12000.00', 1, 'HJY-789', 5),
(17, '2020-01-05', '2024-04-07', 'Mensual', '98000.00', '13000.00', 1, '999-BBB', 7),
(18, '2020-01-06', '2024-04-08', 'Mensual', '99000.00', '18000.00', 1, '888-BBB', 1),
(19, '2020-01-07', '2024-04-09', 'Mensual', '95000.00', '19000.00', 1, 'HMP-710', 5),
(20, '2020-01-08', '2024-04-10', 'Mensual', '95000.00', '21000.00', 1, 'MUI-890', 7),
(21, '2020-01-10', '2024-04-11', 'Mensual', '92000.00', '26000.00', 1, 'ZMP-204', 1),
(22, '2020-01-11', '2024-04-12', 'Mensual', '91000.00', '30000.00', 1, 'CDR-532', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_novedad`
--

CREATE TABLE `detalle_novedad` (
  `ID_DETNOV` bigint(20) NOT NULL,
  `ID_TIPONOV_FK` bigint(20) NOT NULL,
  `ID_NOVEDAD_FK` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `detalle_novedad`
--

INSERT INTO `detalle_novedad` (`ID_DETNOV`, `ID_TIPONOV_FK`, `ID_NOVEDAD_FK`) VALUES
(1, 2, 1),
(2, 3, 1),
(3, 2, 4),
(6, 1, 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrega_ahorro`
--

CREATE TABLE `entrega_ahorro` (
  `ID_ENTREAHORRO` bigint(20) NOT NULL,
  `VALOR_ENTREAHORRO` decimal(9,2) NOT NULL,
  `FECHAINICIO_CONVENIO` date NOT NULL,
  `FECHAFIN_CONVENIO` date NOT NULL,
  `ID_CONVENIO_FK` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `entrega_ahorro`
--

INSERT INTO `entrega_ahorro` (`ID_ENTREAHORRO`, `VALOR_ENTREAHORRO`, `FECHAINICIO_CONVENIO`, `FECHAFIN_CONVENIO`, `ID_CONVENIO_FK`) VALUES
(1, '90000.00', '2019-09-02', '2024-04-03', 1),
(2, '60000.00', '2019-09-02', '2020-09-02', 1),
(3, '90000.00', '2019-09-02', '2020-06-03', 4),
(4, '70000.00', '2019-09-02', '2020-07-03', 5),
(5, '10000.00', '2019-09-02', '2020-11-03', 9),
(6, '70000.00', '2019-09-02', '2020-09-17', 1);

--
-- Disparadores `entrega_ahorro`
--
DELIMITER $$
CREATE TRIGGER `EA_PAGO` AFTER INSERT ON `entrega_ahorro` FOR EACH ROW BEGIN 

INSERT INTO pago_producido (VALOR_PRODUCIDO,FECHA_PRODUCIDO,S_N_PRODUCIDO,S_N_CANCELADO,ID_CONVENIO_FK)
VALUES((-1* NEW.VALOR_ENTREAHORRO), NOW(), 2, 1, 1);

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `ID_ESTADO` bigint(20) NOT NULL,
  `NOM_ESTADO` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`ID_ESTADO`, `NOM_ESTADO`) VALUES
(1, 'Activo'),
(2, 'Inactivo'),
(3, 'En proceso'),
(4, 'Resuelto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `ID_MARCA` bigint(20) NOT NULL,
  `NOM_MARCA` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`ID_MARCA`, `NOM_MARCA`) VALUES
(1, 'Renault'),
(2, 'BMW'),
(3, 'Chevrolet'),
(4, 'Alfa Romeo'),
(5, 'Bugatti'),
(6, 'Bentley'),
(7, 'Alpino'),
(8, 'Ford'),
(9, 'CUPRA'),
(10, 'Jeep');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedad`
--

CREATE TABLE `novedad` (
  `ID_NOVEDAD` bigint(20) NOT NULL,
  `DESCRIP_NOVEDAD` varchar(100) NOT NULL,
  `FECH_NOVEDAD` date NOT NULL,
  `URL_SUPNOV` varchar(300) NOT NULL,
  `VALOR_NOVEDAD` decimal(9,2) NOT NULL,
  `ID_PERSONA_FK` bigint(20) NOT NULL,
  `ID_CLASINOV_FK` bigint(20) NOT NULL,
  `ID_ESTADO_FK` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `novedad`
--

INSERT INTO `novedad` (`ID_NOVEDAD`, `DESCRIP_NOVEDAD`, `FECH_NOVEDAD`, `URL_SUPNOV`, `VALOR_NOVEDAD`, `ID_PERSONA_FK`, `ID_CLASINOV_FK`, `ID_ESTADO_FK`) VALUES
(1, 'Daños', '2002-03-06', 'Www.zzz.com', '70000.00', 1, 1, 3),
(2, 'Robo', '2012-04-03', 'Www.ccc.com', '50000.00', 1, 2, 3),
(3, 'Accidente', '2022-04-02', 'Www.sss.com', '30000.00', 2, 3, 3),
(4, 'Me choqué con un poste en la cra 24.', '2020-09-17', 'Www.choque.com', '89000.00', 9, 3, 3),
(6, 'Robo por San Andresito en una carrera.', '2020-09-17', 'www.robo.com', '78000.00', 9, 2, 3),
(10, 'ergerg', '2020-09-17', 'wewgwg', '537437.00', 9, 1, 3),
(11, 'ergerg', '2020-09-17', 'wewgwg', '537437.00', 9, 1, 3),
(12, 'trtrjr', '2020-09-17', 'fgfjfj', '77777.00', 9, 1, 3),
(13, 'trtrjr', '2020-09-17', 'fgfjfj', '77777.00', 9, 1, 3),
(14, 'trtrjr', '2020-09-17', 'fgfjfj', '77777.00', 9, 1, 3),
(15, 'ergerg', '2020-09-17', 'dfbdbdb', '525252.00', 9, 1, 3),
(16, 'ergerg', '2020-09-17', 'dfbdbdb', '525252.00', 9, 1, 3),
(17, 'ergerg', '2020-09-17', 'sdvdvw', '72727.00', 9, 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_producido`
--

CREATE TABLE `pago_producido` (
  `ID_PRODUCIDO` bigint(20) NOT NULL,
  `VALOR_PRODUCIDO` decimal(9,2) NOT NULL,
  `FECHA_PRODUCIDO` datetime NOT NULL,
  `S_N_PRODUCIDO` tinyint(1) DEFAULT NULL,
  `S_N_CANCELADO` tinyint(1) DEFAULT NULL,
  `ID_CONVENIO_FK` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pago_producido`
--

INSERT INTO `pago_producido` (`ID_PRODUCIDO`, `VALOR_PRODUCIDO`, `FECHA_PRODUCIDO`, `S_N_PRODUCIDO`, `S_N_CANCELADO`, `ID_CONVENIO_FK`) VALUES
(1, '90000.00', '2020-08-15 00:00:00', 1, 2, 1),
(2, '60000.00', '2020-09-02 00:00:00', 2, 2, 1),
(5, '80000.00', '2020-05-03 00:00:00', 1, 1, 3),
(6, '90000.00', '2020-06-03 00:00:00', 2, 2, 4),
(7, '70000.00', '2020-07-03 00:00:00', 1, 2, 5),
(8, '40000.00', '2020-08-03 00:00:00', 2, 1, 6),
(9, '80000.00', '2020-09-03 00:00:00', 1, 1, 7),
(11, '10000.00', '2020-11-03 00:00:00', 2, 2, 9),
(12, '50000.00', '2020-04-03 00:00:00', 2, 1, 10),
(13, '-60000.00', '2020-09-16 23:17:00', 2, 1, 1),
(14, '-90000.00', '2020-09-16 23:17:00', 2, 1, 1),
(15, '-70000.00', '2020-09-16 23:17:00', 2, 1, 1),
(16, '-10000.00', '2020-09-16 23:17:00', 2, 1, 1),
(17, '500000.00', '2020-09-16 00:00:00', 2, 1, 1),
(18, '-70000.00', '2020-09-17 00:04:36', 2, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `ID_PERSONA` bigint(20) NOT NULL,
  `DOCU_PERSONA` decimal(10,0) NOT NULL,
  `NOM_PERSONA` varchar(20) NOT NULL,
  `APE_PERSONA` varchar(20) NOT NULL,
  `TEL_PERSONA` decimal(10,0) NOT NULL,
  `CORREO_PERSONA` varchar(40) NOT NULL,
  `DIREC_PERSONA` varchar(30) NOT NULL,
  `ID_USUARIO_FK` bigint(20) NOT NULL,
  `ID_ROL_FK` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`ID_PERSONA`, `DOCU_PERSONA`, `NOM_PERSONA`, `APE_PERSONA`, `TEL_PERSONA`, `CORREO_PERSONA`, `DIREC_PERSONA`, `ID_USUARIO_FK`, `ID_ROL_FK`) VALUES
(1, '1000423456', 'David', 'Palacios', '3222275804', 'David27@gmail.com', 'K72 Q', 1, 3),
(2, '1000342348', 'Christian', 'Ruiz', '2312233221', 'christian@gmail.com', 'Calle 45', 2, 2),
(3, '1231112333', 'William', 'Ochoa', '3221444321', 'william@gmail.com', 'Cra 59', 3, 1),
(4, '1000579838', 'Manuel', 'Quiroz', '3548698', 'manuel@gmail.com', 'Calle 44', 4, 1),
(5, '1009324321', 'Philips', 'Cambell', '3221144552', 'baiken@gmail.com', 'Calle...', 5, 3),
(6, '1004429932', 'Margaret', 'Martinez', '3221144532', 'margaret@gmail.com', 'Calle...', 6, 2),
(7, '1998342124', 'Carlos', 'Ruiz', '3212312542', 'white378@gmail.com', 'Calle...', 7, 3),
(8, '1299123882', 'Luis', 'Baskes', '3421233123', 'blink213@gmail.com', 'Calle...', 8, 2),
(9, '1888223199', 'Camilo', 'Olivera', '3224412412', 'camilo@gmail.com', 'Calle...', 9, 3),
(10, '1777439902', 'Manuel', 'Moncada', '3221144212', 'manuelmoncada151@gmail.com', 'Calle...', 10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `ID_ROL` bigint(20) NOT NULL,
  `NOM_ROL` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`ID_ROL`, `NOM_ROL`) VALUES
(1, 'Administrador'),
(2, 'Socio'),
(3, 'Conductor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_novedad`
--

CREATE TABLE `tipo_novedad` (
  `ID_TIPONOV` bigint(20) NOT NULL,
  `NOM_TIPONOV` varchar(25) NOT NULL,
  `ID_CLASINOV_FK` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_novedad`
--

INSERT INTO `tipo_novedad` (`ID_TIPONOV`, `NOM_TIPONOV`, `ID_CLASINOV_FK`) VALUES
(1, 'Cambio de aceite', 1),
(2, 'Choque', 3),
(3, 'Espejo', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `ID_USUARIO` bigint(20) NOT NULL,
  `NOM_USUARIO` varchar(35) NOT NULL,
  `CONTRA_USUARIO` varchar(20) NOT NULL,
  `ID_ROL_FK` bigint(20) NOT NULL,
  `ID_ESTADO_FK` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`ID_USUARIO`, `NOM_USUARIO`, `CONTRA_USUARIO`, `ID_ROL_FK`, `ID_ESTADO_FK`) VALUES
(1, 'David27@gmail.com', '123', 3, 1),
(2, 'chris@gmail.com', '456', 2, 1),
(3, 'flowblack22@gmail.com', '789', 1, 1),
(4, 'rapingo@gmail.com', '147', 1, 1),
(5, 'baiken2@gmail.com', '1890213', 3, 1),
(6, 'margaret@gmail.com', 'Velvet Room', 2, 1),
(7, 'white378@gmail.com', 'White in black', 3, 1),
(8, 'blink213@gmail.com', 'Password', 2, 1),
(9, 'camilo@gmail.com', '123', 3, 1),
(10, 'manuelmoncada151@gmail.com', '147', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo`
--

CREATE TABLE `vehiculo` (
  `ID_VEHICULO` bigint(20) NOT NULL,
  `PLACA_VEHICULO` varchar(7) DEFAULT NULL,
  `MODELO_VEHICLO` varchar(10) NOT NULL,
  `FECHAING_VEHICULO` date NOT NULL,
  `ID_MARCA_FK` bigint(20) NOT NULL,
  `ID_PERSONA_FK` bigint(20) NOT NULL,
  `ID_ESTADO_FK` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `vehiculo`
--

INSERT INTO `vehiculo` (`ID_VEHICULO`, `PLACA_VEHICULO`, `MODELO_VEHICLO`, `FECHAING_VEHICULO`, `ID_MARCA_FK`, `ID_PERSONA_FK`, `ID_ESTADO_FK`) VALUES
(1, '666-BBB', '2014', '2002-09-09', 1, 2, 1),
(2, '888-BBB', '2016', '2003-09-09', 1, 2, 1),
(3, '999-BBB', '2015', '2004-09-09', 1, 2, 1),
(4, 'HJY-789', '2012', '2012-09-09', 2, 3, 1),
(5, 'HMP-710', '1999', '2000-05-05', 3, 4, 1),
(6, 'ZMP-204', '2002', '2003-03-09', 4, 5, 2),
(7, 'MUI-890', '2018', '2018-06-06', 5, 6, 1),
(8, 'GOP-562', '2000', '2001-11-22', 6, 7, 1),
(9, 'TOP-662', '1994', '1999-12-12', 7, 8, 1),
(10, 'CDR-532', '1997', '1999-02-03', 8, 9, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clasificacion_novedad`
--
ALTER TABLE `clasificacion_novedad`
  ADD PRIMARY KEY (`ID_CLASINOV`);

--
-- Indices de la tabla `convenio_producido`
--
ALTER TABLE `convenio_producido`
  ADD PRIMARY KEY (`ID_CONVENIO`),
  ADD KEY `PLACA_VEHICULO_FK` (`PLACA_VEHICULO_FK`),
  ADD KEY `ID_PERSONA_FK` (`ID_PERSONA_FK`);

--
-- Indices de la tabla `detalle_novedad`
--
ALTER TABLE `detalle_novedad`
  ADD PRIMARY KEY (`ID_DETNOV`),
  ADD KEY `ID_TIPONOV_FK` (`ID_TIPONOV_FK`),
  ADD KEY `ID_NOVEDAD_FK` (`ID_NOVEDAD_FK`);

--
-- Indices de la tabla `entrega_ahorro`
--
ALTER TABLE `entrega_ahorro`
  ADD PRIMARY KEY (`ID_ENTREAHORRO`),
  ADD KEY `ID_CONVENIO_FK` (`ID_CONVENIO_FK`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`ID_ESTADO`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`ID_MARCA`);

--
-- Indices de la tabla `novedad`
--
ALTER TABLE `novedad`
  ADD PRIMARY KEY (`ID_NOVEDAD`),
  ADD KEY `ID_PERSONA_FK` (`ID_PERSONA_FK`),
  ADD KEY `ID_ESTADO_FK` (`ID_ESTADO_FK`),
  ADD KEY `ID_CLASINOV_FK` (`ID_CLASINOV_FK`);

--
-- Indices de la tabla `pago_producido`
--
ALTER TABLE `pago_producido`
  ADD PRIMARY KEY (`ID_PRODUCIDO`),
  ADD KEY `ID_CONVENIO_FK` (`ID_CONVENIO_FK`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`ID_PERSONA`),
  ADD UNIQUE KEY `DOCU_PERSONA` (`DOCU_PERSONA`),
  ADD UNIQUE KEY `CORREO_PERSONA` (`CORREO_PERSONA`),
  ADD UNIQUE KEY `ID_USUARIO_FK` (`ID_USUARIO_FK`),
  ADD KEY `ID_ROL_FK` (`ID_ROL_FK`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`ID_ROL`);

--
-- Indices de la tabla `tipo_novedad`
--
ALTER TABLE `tipo_novedad`
  ADD PRIMARY KEY (`ID_TIPONOV`),
  ADD KEY `ID_CLASINOV_FK` (`ID_CLASINOV_FK`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`ID_USUARIO`),
  ADD UNIQUE KEY `NOM_USUARIO` (`NOM_USUARIO`),
  ADD KEY `ID_ROL_FK` (`ID_ROL_FK`),
  ADD KEY `ID_ESTADO_FK` (`ID_ESTADO_FK`);

--
-- Indices de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD PRIMARY KEY (`ID_VEHICULO`),
  ADD UNIQUE KEY `PLACA_VEHICULO` (`PLACA_VEHICULO`),
  ADD KEY `ID_MARCA_FK` (`ID_MARCA_FK`),
  ADD KEY `ID_PERSONA_FK` (`ID_PERSONA_FK`),
  ADD KEY `ID_ESTADO_FK` (`ID_ESTADO_FK`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clasificacion_novedad`
--
ALTER TABLE `clasificacion_novedad`
  MODIFY `ID_CLASINOV` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `convenio_producido`
--
ALTER TABLE `convenio_producido`
  MODIFY `ID_CONVENIO` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `detalle_novedad`
--
ALTER TABLE `detalle_novedad`
  MODIFY `ID_DETNOV` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `entrega_ahorro`
--
ALTER TABLE `entrega_ahorro`
  MODIFY `ID_ENTREAHORRO` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `ID_ESTADO` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `ID_MARCA` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `novedad`
--
ALTER TABLE `novedad`
  MODIFY `ID_NOVEDAD` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `pago_producido`
--
ALTER TABLE `pago_producido`
  MODIFY `ID_PRODUCIDO` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `ID_PERSONA` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `ID_ROL` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_novedad`
--
ALTER TABLE `tipo_novedad`
  MODIFY `ID_TIPONOV` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `ID_USUARIO` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  MODIFY `ID_VEHICULO` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `convenio_producido`
--
ALTER TABLE `convenio_producido`
  ADD CONSTRAINT `convenio_producido_ibfk_1` FOREIGN KEY (`PLACA_VEHICULO_FK`) REFERENCES `vehiculo` (`PLACA_VEHICULO`) ON DELETE CASCADE,
  ADD CONSTRAINT `convenio_producido_ibfk_2` FOREIGN KEY (`ID_PERSONA_FK`) REFERENCES `persona` (`ID_PERSONA`) ON DELETE CASCADE;

--
-- Filtros para la tabla `detalle_novedad`
--
ALTER TABLE `detalle_novedad`
  ADD CONSTRAINT `detalle_novedad_ibfk_1` FOREIGN KEY (`ID_TIPONOV_FK`) REFERENCES `tipo_novedad` (`ID_TIPONOV`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalle_novedad_ibfk_2` FOREIGN KEY (`ID_NOVEDAD_FK`) REFERENCES `novedad` (`ID_NOVEDAD`) ON DELETE CASCADE;

--
-- Filtros para la tabla `entrega_ahorro`
--
ALTER TABLE `entrega_ahorro`
  ADD CONSTRAINT `entrega_ahorro_ibfk_1` FOREIGN KEY (`ID_CONVENIO_FK`) REFERENCES `convenio_producido` (`ID_CONVENIO`) ON DELETE CASCADE;

--
-- Filtros para la tabla `novedad`
--
ALTER TABLE `novedad`
  ADD CONSTRAINT `novedad_ibfk_1` FOREIGN KEY (`ID_PERSONA_FK`) REFERENCES `persona` (`ID_PERSONA`) ON DELETE CASCADE,
  ADD CONSTRAINT `novedad_ibfk_2` FOREIGN KEY (`ID_ESTADO_FK`) REFERENCES `estado` (`ID_ESTADO`) ON DELETE CASCADE,
  ADD CONSTRAINT `novedad_ibfk_3` FOREIGN KEY (`ID_CLASINOV_FK`) REFERENCES `clasificacion_novedad` (`ID_CLASINOV`) ON DELETE CASCADE;

--
-- Filtros para la tabla `pago_producido`
--
ALTER TABLE `pago_producido`
  ADD CONSTRAINT `pago_producido_ibfk_1` FOREIGN KEY (`ID_CONVENIO_FK`) REFERENCES `convenio_producido` (`ID_CONVENIO`) ON DELETE CASCADE;

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`ID_ROL_FK`) REFERENCES `rol` (`ID_ROL`) ON DELETE CASCADE,
  ADD CONSTRAINT `persona_ibfk_2` FOREIGN KEY (`ID_USUARIO_FK`) REFERENCES `usuario` (`ID_USUARIO`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tipo_novedad`
--
ALTER TABLE `tipo_novedad`
  ADD CONSTRAINT `tipo_novedad_ibfk_1` FOREIGN KEY (`ID_CLASINOV_FK`) REFERENCES `clasificacion_novedad` (`ID_CLASINOV`) ON DELETE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`ID_ROL_FK`) REFERENCES `rol` (`ID_ROL`) ON DELETE CASCADE,
  ADD CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`ID_ESTADO_FK`) REFERENCES `estado` (`ID_ESTADO`) ON DELETE CASCADE;

--
-- Filtros para la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD CONSTRAINT `vehiculo_ibfk_1` FOREIGN KEY (`ID_MARCA_FK`) REFERENCES `marca` (`ID_MARCA`) ON DELETE CASCADE,
  ADD CONSTRAINT `vehiculo_ibfk_2` FOREIGN KEY (`ID_PERSONA_FK`) REFERENCES `persona` (`ID_PERSONA`) ON DELETE CASCADE,
  ADD CONSTRAINT `vehiculo_ibfk_3` FOREIGN KEY (`ID_ESTADO_FK`) REFERENCES `estado` (`ID_ESTADO`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
