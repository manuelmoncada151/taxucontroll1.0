

$("#submit").click(function(e){
	e.preventDefault()
	//e.preventDefault()
	if(	$("#PLACA_VEHICULO").val() == null || $("#PLACA_VEHICULO").val() == ""  ||
		$("#MODELO_VEHICLO").val() == null || $("#MODELO_VEHICLO").val() == "" ||
		$("#FECHAING_VEHICULO").val() == null || $("#FECHAING_VEHICULO").val() == "" ||
		$("#ID_MARCA_FK").val() == null || $("#ID_MARCA_FK").val() == "" ||
		$("#ID_PERSONA_FK").val() == null || $("#ID_PERSONA_FK").val() == ""  ){
		swal({
                title: "Espera!",
                text: "No puedes dejar los campos vacíos!",
                icon: "warning",
                button: "Volver",
              });
	return false;
	}

	let url="?controller=veh&method=save"
	
	let params = {
		PLACA_VEHICULO: $("#PLACA_VEHICULO").val(),
		MODELO_VEHICLO: $("#MODELO_VEHICLO").val(),
		FECHAING_VEHICULO: $("#FECHAING_VEHICULO").val(),
		ID_MARCA_FK: $("#ID_MARCA_FK").val(),
		ID_PERSONA_FK: $("#ID_PERSONA_FK").val()
	}
	$.post(url,params,function(response){
		console.log(confirm)
		if($confirm!==true){
			alert(response.error)

		}else{


			alert("Insercion satisfactoria")
			location.href = "?controller=veh"

		}
	}, 'json').fail(function(error){
		alert("Insercion satisfactoria")
		location.href = "?controller=veh"
	
		console.log(confirm)
	});
});

function soloNumeros(e){

    key=e.keyCode || e.which;

    teclado = String.fromCharCode(key);
    numero ="0123456789";
    especiales="8-37-38-46";//array
    teclado_especial= false;

    for(var i in especiales){
        if(key==especiales[i]){
            teclado_especial=true;
        }
    }

    if(numero.indexOf(teclado)==-1 && !teclado_especial){
        swal({
            title: "Espera!",
            text: "Solo se permiten Números!",
            icon: "error",
            button: "Volver",
          });
        return false;
    }
}