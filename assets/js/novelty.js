$(document).ready(function(){
  $.ajax({
    type: 'POST',
    url: 'views/novelty/select/cargar_classif.php'
  })
  .done(function(listas_rep){
    $('#ID_CLASINOV_FK').html(listas_rep)
  })
  .fail(function(){
    alert('Hubo un errror al cargar las clasificaciones')
   })

  $('#ID_CLASINOV_FK').on('change', function(){
    var id = $('#ID_CLASINOV_FK').val()
    $.ajax({
      type: 'POST',
      url: 'views/novelty/select/cargar_tipos.php',
      data: {'id': id}
    })
    .done(function(listas_rep){
      $('#tipos').html(listas_rep)
    })
    .fail(function(){
      alert('Hubo un errror al cargar los tipos')
    })
  })

}) 
//declarar Array global que contenga la lista de categorias
// var arrCategories =[]
showTypes()
//llamar funcion de jquery para accion click de boton addElement
$("#addElement").click(function (e) {
	e.preventDefault()

	let ID_TIPONOV =$("#tipos").val()
	let NOM_TIPONOV = $("#tipos option:selected").text()

	if (ID_TIPONOV != '') {
		if (ID_TIPONOV===null){
			swal({
                title: "Espera!",
                text: "Debes seleccionar una clasificación!",
                icon: "warning",
                button: "Volver",
              });
		}else{
			if(typeof existType(ID_TIPONOV) === 'undefined') {

			arrTypes.push({
			'id': ID_TIPONOV,
			'name': NOM_TIPONOV
		})
		}else{
			swal({
                title: "Espera!",
                text: "El tipo ya se encuentra seleccionado!",
                icon: "warning",
                button: "Volver",
              });
		}
		}
		
		
		showTypes()
	}else{
		swal({
                title: "Espera!",
                text: "Debes seleccionar un tipo!",
                icon: "warning",
                button: "Volver",
              });
	}
	
});

function existType(ID_TIPONOV){
	let existType = arrTypes.find(function(type){
		return type.id == ID_TIPONOV
	})
	return existType
}

function showTypes(){
	$("#list-types").empty()

	arrTypes.forEach(function(type){
		$("#list-types").append('<div class="form-group"><button onclick="removeElement('+type.id+')" class="btn btn-danger">X</button><span>'+type.name+'</span></div>')
	})
}

function removeElement(ID_TIPONOV){
	let index = arrTypes.indexOf(existType(ID_TIPONOV))
	arrTypes.splice(index,1)
	showTypes()
}

$("#submit").click(function(e){
	e.preventDefault()
	//e.preventDefault()
	var prueba = document.getElementById('Activo').checked;

	if(	$("#DESCRIP_NOVEDAD").val() == null || $("#DESCRIP_NOVEDAD").val() == ""  ||
		$("#FECH_NOVEDAD").val() == null || $("#FECH_NOVEDAD").val() == "" ||
		$("#URL_SUPNOV").val() == null || $("#URL_SUPNOV").val() == "" ||
		$("#VALOR_NOVEDAD").val() == null || $("#VALOR_NOVEDAD").val() == "" ||
		$("#ID_PERSONA_FK").val() == null || $("#ID_PERSONA_FK").val() == "" ||
		$("#ID_CLASINOV_FK").val() == null || $("#ID_CLASINOV_FK").val() == "" ){
		swal({
                title: "Espera!",
                text: "No puedes dejar los campos vacíos!",
                icon: "warning",
                button: "Volver",
              });
	return false;
	}

	let url="?controller=novelty&method=save"
	
	let params = {
		DESCRIP_NOVEDAD: $("#DESCRIP_NOVEDAD").val(),
		FECH_NOVEDAD: $("#FECH_NOVEDAD").val(),
		URL_SUPNOV: $("#URL_SUPNOV").val(),
		VALOR_NOVEDAD: $("#VALOR_NOVEDAD").val(),
		ID_PERSONA_FK: $("#ID_PERSONA_FK").val(),
		ID_CLASINOV_FK: $("#ID_CLASINOV_FK").val(),
		ID_ESTADO_FK: prueba,
		vehiculo: $("#veh1").val(),
		types: arrTypes
	}
	$.post(url,params,function(response){
		if(typeof response.error !== 'undefined'){
			alert(response.error)
		}else{
			swal({
                title: "Perfecto!",
                text: "Inserción satisfactoria!",
                icon: "warning",
                button: "Volver",
              });
			location.href = "?controller=novelty"
		}
	}, 'json').fail(function(error){
		alert("Actualización Fallida ("+error.responseText+")")
		console.log(error)
	});
});
