<?php
	
	/**
	 * Modelo de la Tabla users
	 */
	class Veh
	{
		private $ID_VEHICULO;
		private $PLACA_VEHICULO;
		private $MODELO_VEHICULO;
		private $FECHAING_VEHICULO;
		private $ID_MARCA_FK;
		private $ID_PERSONA_FK;
		private $ID_ESTADO_FK;
		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT v. *, m.NOM_MARCA as NOM_MARCA,  P.NOM_PERSONA as NOM_PERSONA from vehiculo v INNER JOIN marca m ON m.ID_MARCA=v.ID_MARCA_FK INNER JOIN persona P ON P.ID_PERSONA=v.ID_PERSONA_FK 
				ORDER BY v.ID_VEHICULO";

				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function newVeh($data)
		{
			try {
				$data['ID_ESTADO_FK'] = 1;
		
				$this->pdo->insert('vehiculo', $data);
				return true;				
			} catch(PDOException $e) {

				die($e->getMessage());
			}	
		}

		public function getVehById($ID_VEHICULO)
		{
			try {
				$strSql = "SELECT * FROM vehiculo WHERE ID_VEHICULO = :id";
				$arrayData = ['id' => $ID_VEHICULO];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function editVeh($data)
		{
			try {
			
				$strWhere = 'ID_VEHICULO = '. $data['ID_VEHICULO'];
				$this->pdo->update('vehiculo', $data, $strWhere);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}		
		}

		public function deleteVeh($data)
		{
			try {
				$strWhere = 'ID_VEHICULO = '. $data['ID_VEHICULO'];
				$this->pdo->delete('vehiculo', $strWhere);
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}
	}