<?php

	/**
	 * Modelo de login
	 */


	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require 'assets/PHPMailer/Exception.php';
	require 'assets/PHPMailer/PHPMailer.php';
	require 'assets/PHPMailer/SMTP.php';

	class Login
	{

		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		

		public function validateEmail($correo)
		{
			try {
				$correo;
				$strSql = "SELECT U.ID_USUARIO as usuario FROM usuario U WHERE U.NOM_USUARIO='$correo'";
				$query = $this->pdo->select($strSql);
				if (isset($query[0]->usuario)) {
					return true;
				} else {
					return false;
				}

			} catch(PDOException $e) {
				return $e->getMessage();
			}
		}

		public function updatepassword($info)
		{
			try {
				$data=$info[1];
				$strWhere =$info[0];
				/* MySQL Conexion*/
				$link = mysqli_connect("localhost", "root", '', "taxucontroll");
			    // Chequea coneccion
				if($link === false){
					die("ERROR: No pudo conectarse con la DB. " . mysqli_connect_error());
				}
			    // Ejecuta la actualizacion del registro
				$sql = "UPDATE usuario SET CONTRA_USUARIO = '$data' WHERE NOM_USUARIO='$strWhere'";
				if(mysqli_query($link, $sql)){
			        //echo "Registro actualizado.";
				} else {
					echo "ERROR: No se ejecuto $sql. " . mysqli_error($link);
				}
			    // Cierra la conexion
				mysqli_close($link);		
				return true;
			} catch(PDOException $e) {	
				die($e->getMessage());
			}		
		}


		public function validateUser($data)
		{
			try {
				$strSql = "SELECT u.*,s.NOM_ESTADO as status, r.NOM_ROL as role,CONCAT(p.NOM_PERSONA,' ',p.APE_PERSONA) as nombre,p.ID_PERSONA FROM usuario u 
INNER JOIN estado s ON s.ID_ESTADO =u.ID_ESTADO_FK 
INNER JOIN rol r ON r.ID_ROL =u.ID_ROL_FK
INNER JOIN persona p on p.ID_PERSONA=u.ID_USUARIO WHERE u.NOM_USUARIO='{$data['nombre']}' AND u.CONTRA_USUARIO = '{$data['password']}'";

				$query = $this->pdo->select($strSql);

				if (isset($query[0]->ID_USUARIO)) {
					if ($query[0]->ID_ESTADO_FK ==1) {

						$_SESSION['user'] = $query[0];
						return true;

					} else {
						return 'Error al Iniciar Sesión. Su usuario está inactivo';
					}
				} else {
					return 'Error al Iniciar Sesión. Verifique sus credenciales';
				}
				

			} catch(PDOException $e) {
				return $e->getMessage();
			}
		}


		public function getperson(){
			try {
				$user=$_SESSION['user']->ID_USUARIO;
				$strSql= "SELECT p.*,r.NOM_ROL as role,e.NOM_ESTADO as estado, u.ID_USUARIO as usuario FROM `persona` p
				INNER JOIN rol r ON r.ID_ROL =p.ID_ROL_FK
				INNER JOIN usuario u on u.ID_USUARIO=p.ID_USUARIO_FK
				INNER JOIN estado e ON E.ID_ESTADO=u.ID_ESTADO_FK
				WHERE p.ID_USUARIO_FK = $user";
				$query = $this->pdo->select($strSql);
				return $query;
			} catch(PDOException $e) {
				return $e->getMessage();
			}
		}

		public function getAdmin(){
			try {
				$user=$_SESSION['user']->ID_USUARIO;
				$strSql= "SELECT p.*,r.NOM_ROL as role,e.NOM_ESTADO as estado, u.ID_USUARIO as usuario,CONCAT(NOM_PERSONA,' ', APE_PERSONA)AS NOMBRE FROM `persona` p
				INNER JOIN rol r ON r.ID_ROL =p.ID_ROL_FK
				INNER JOIN usuario u on u.ID_USUARIO=p.ID_USUARIO_FK
				INNER JOIN estado e ON E.ID_ESTADO=u.ID_ESTADO_FK
				WHERE u.ID_ROL_FK=1";
				$query = $this->pdo->select($strSql);
				return $query;
			} catch(PDOException $e) {
				return $e->getMessage();
			}
		}


		public function random_password()  
		{  
		  $longitud = 8; // longitud del password  
		  $pass = substr(md5(rand()),0,$longitud);  
		  return($pass); // devuelve el password   
		}

		public function sendpassword(){

			try {
				

				$nombre = $_POST['name'];
				$correo = $_POST['correo'];


	// Instantiation and passing `true` enables exceptions
				$mail = new PHPMailer(true);
				$this->model = new Login;
				try {
					$longitud = 8; 
					$pass = substr(md5(rand()),0,$longitud);

					$mail->SMTPOptions = array(
						'ssl' => array(
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
						)
					);
			    //Server settings
			    $mail->SMTPDebug = 0;                      // Enable verbose debug output
			    $mail->isSMTP();                                            // Send using SMTP
			    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
			    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			    $mail->Username   = 'taxucontrollsn@gmail.com';                     // SMTP username
			    $mail->Password   = 'taxucontroll2411';                               // SMTP password
			    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

			    //Recipients
			    $mail->setFrom('taxucontroll@gmail.com', 'Taxucontroll');
			    $mail->addAddress($correo, $nombre);     // Add a recipient
			    // $mail->addAddress('ellen@example.com');               // Name is optional
			    // $mail->addReplyTo('info@example.com', 'Information');
			    // $mail->addCC('cc@example.com');
			    // $mail->addBCC('bcc@example.com');

			    // Attachments
			    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			    // Content
			    $mail->isHTML(true);                                  // Set email format to HTML
			    $mail->Subject = 'Bienvenido a taxucontroll '.$nombre ;
			    $mail->Body    = "<!DOCTYPE html>
			    <html>
			    <head>
			    <meta charset='utf-8'>
			    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
			    </head>
			    <body>
			    <style type='text/css'>
			    .left {
			    	float: center;
			    	width: 125px;
			    	text-align: right;
			    	margin-top: 10px;
			    	margin-right: 200px;
			    	display: inline;
			    }

			    .right {
			    	float: left;
			    	text-align: center;
			    	margin-top: 5px;
			    	display: inline;
			    }
			    </style>
			    <table style='border: 0px solid black; width: 100%;'>
			    <thead>
			    <tr>
			    <td style='text-align: center;background-color: #FFD518;color: black; height: 100px' colspan='2'>
			    <div class='left'>
			    <img style='' src='https://i.ibb.co/8NsZ7Sd/tax8.png'>
			    </div>
			    <div class='right'>
			    <h1><b>Hola $nombre</b></h1>
			    </div>
			    </td>
			    </tr>
			    <tr>

			    <td style='text-align: left;' colspan='4'><span style='font-size: 25px'>Has solicitado un cambio de clave</span>  
			    <span style='font-size: 25px;'><p>
			    <br>
			    Hola, Taxucontroll esta contigo en las buenas y en las malas para hacertelas mejores...   
			    Si tu no has solicitado este cambio de contraseña por favor comunicate con tu administrador.
			    <br>
			    Estamos contigo,
			    <br>
			    <br>
			    <br>
			    <span style='color:#222222;'><strong>By:Taxucontroll</strong></span></td>
			    </p>
			    </span>
			    </td>
			    </tr>
			    <td style='text-align: center;background-color: black;color: white;' colspan='2'>
			    <h2><b>Tu nueva clave es:$pass</b></h2>
			    </td>
			    </tr>
			    </thead>
			    </table>
			    </body>
			    </html>";
			    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			    $mail->send();


			    return $pass;

			} catch (Exception $e) {
				echo 0;
				return $e->getMessage();
			}
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function Notificacion($arrnotify,$admins){

		try {
			$nombreco=$arrnotify[0];
			$pago=$arrnotify[1];
				
				
	// Instantiation and passing `true` enables exceptions
				$mail = new PHPMailer(true);
				$this->model = new Login;
				try {

					foreach ($admins as $admin ) {
					$nombre = $admin->NOMBRE;
					$correo = $admin->CORREO_PERSONA;
					$longitud = 8; 
					$pass = substr(md5(rand()),0,$longitud);

					$mail->SMTPOptions = array(
						'ssl' => array(
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
						)
					);

			    //Server settings
			    $mail->SMTPDebug = 0;                      // Enable verbose debug output
			    $mail->isSMTP();                                            // Send using SMTP
			    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
			    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			    $mail->Username   = 'taxucontrollsn@gmail.com';                     // SMTP username
			    $mail->Password   = 'taxucontroll2411';                               // SMTP password
			    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

			    //Recipients
			    $mail->setFrom('taxucontroll@gmail.com', 'Taxucontroll');
			    $mail->addAddress($correo, $nombre);     // Add a recipient
			    // $mail->addAddress('ellen@example.com');               // Name is optional
			    // $mail->addReplyTo('info@example.com', 'Information');
			    // $mail->addCC('cc@example.com');
			    // $mail->addBCC('bcc@example.com');

			    // Attachments
			    // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			    // Content
			    $mail->isHTML(true);                                  // Set email format to HTML
			    $mail->Subject = 'Tienes un pago por confirmar o cancelar' ;
			    $mail->Body    = "<!DOCTYPE html>
			    <html>
			    <head>
			    <meta charset='utf-8'>
			    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
			    </head>
			    <body>
			    <style type='text/css'>
			    .left {
			    	float: center;
			    	width: 125px;
			    	text-align: right;
			    	margin-top: 10px;
			    	margin-right: 200px;
			    	display: inline;
			    }

			    .right {
			    	float: left;
			    	text-align: center;
			    	margin-top: 5px;
			    	display: inline;
			    }
			    </style>
			    <table style='border: 0px solid black; width: 100%;'>
			    <thead>
			    <tr>
			    <td style='text-align: center;background-color: #FFD518;color: black; height: 100px' colspan='2'>
			    <div class='left'>
			    <img style='' src='https://i.ibb.co/8NsZ7Sd/tax8.png'>
			    </div>
			    <div class='right'>
			    <h1><b>Hola $nombre</b></h1>
			    </div>
			    </td>
			    </tr>
			    <tr>

			    <td style='text-align: left;' colspan='4'><span style='font-size: 25px'>Se ha registrado un nuevo pago</span>  
			    <span style='font-size: 25px;'><p>
			    <br>
			    El conductor <strong>$nombreco</strong>
			    Ha registrado un nuevo pago de <strong> $pago </strong>
			    <br>
			    Revisa tu cuenta y confirma o rechaza estos pagos, asi sera mas facil llevar un reporte de los mismos.
			    <br>
			    Estamos contigo,
			    <br>
			    <br>
			    <br>
			    <span style='color:#222222;'><strong>By:Taxucontroll</strong></span></td>
			    </p>
			    </span>
			    </td>
			    </tr>
			    <td style='text-align: center;background-color: black;color: white; height: 100px' colspan='2'>

			    </td>
			    </tr>
			    </thead>
			    </table>
			    </body>
			    </html>";
			    // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
			    $mail->send();

				}
			} catch (Exception $e) {
				echo 0;
				return $e->getMessage();
			}

		

	} catch (Exception $e) {
		return $e->getMessage();
	}

}  


}