<?php
/**
 *
 */
class Payment
{

    public function __construct()
    {
        try {
            $this->pdo = new Database;

        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $strSql = "SELECT p.ID_PRODUCIDO,TRUNCATE(p.VALOR_PRODUCIDO,0) as PAGO,p.S_N_CANCELADO,p.S_N_PRODUCIDO,P.FECHA_PRODUCIDO AS FECHA_PRODUCIDO, P.ID_CONVENIO_FK AS CONVENIO,CONCAT(PE.NOM_PERSONA,' ',PE.APE_PERSONA)AS NOMBRE FROM pago_producido p
                INNER JOIN convenio_producido C ON C.ID_CONVENIO=P.ID_CONVENIO_FK
                INNER JOIN persona PE ON PE.ID_PERSONA=C.ID_PERSONA_FK  ORDER BY p.ID_PRODUCIDO DESC";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getAprox()
    {
        try {
            $strSql = "SELECT SUM(c.VALOR_CONVENIO) as Aprox FROM convenio_producido c";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAllById()
    {
        try {
            $user=$_SESSION['user']->ID_USUARIO;
            $strSql = "SELECT P.ID_PRODUCIDO,TRUNCATE(p.VALOR_PRODUCIDO,0) as PAGO,p.S_N_CANCELADO,p.S_N_PRODUCIDO,P.FECHA_PRODUCIDO AS FECHA_PRODUCIDO,c.VALOR_CONVENIO AS  PRODUCIDO,c.AHORRO_PRODUCIDO AS AHORRO, P.ID_CONVENIO_FK AS CONVENIO,PE.ID_PERSONA AS PERSONA, U.ID_USUARIO AS USUARIO,CONCAT(PE.NOM_PERSONA,' ',PE.APE_PERSONA)AS NOMBRE  FROM pago_producido p 
            INNER JOIN convenio_producido C ON C.ID_CONVENIO=P.ID_CONVENIO_FK
            INNER JOIN persona PE ON PE.ID_PERSONA=C.ID_PERSONA_FK
            INNER JOIN usuario U ON U.ID_USUARIO=PE.ID_USUARIO_FK WHERE ID_USUARIO=$user ORDER BY p.ID_PRODUCIDO DESC ";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }


    public function getTotalPaymentById()
    {
        try {
            $user=$_SESSION['user']->ID_USUARIO;
            $strSql = "SELECT p.ID_CONVENIO AS CONVENIO,TRUNCATE(SUM(VALOR_PRODUCIDO),0) AS PRODUCIDO,a.S_N_CANCELADO,p.ID_PERSONA_FK AS PERSONA,u.ID_USUARIO AS USUARIO FROM convenio_producido p 
            INNER JOIN pago_producido a on a.ID_CONVENIO_FK=P.ID_CONVENIO
            INNER JOIN persona pe on pe.ID_PERSONA=p.ID_PERSONA_FK
            INNER JOIN usuario u on u.ID_USUARIO=pe.ID_USUARIO_FK
            WHERE S_N_PRODUCIDO=1 AND u.ID_USUARIO=$user AND a.S_N_CANCELADO=1;";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

     public function getTotalPayment()
    {
        try {
            $user=$_SESSION['user']->ID_USUARIO;
            $strSql = "SELECT TRUNCATE(SUM(VALOR_PRODUCIDO),0) AS PRODUCIDO FROM pago_producido WHERE S_N_PRODUCIDO=1 AND S_N_CANCELADO=1";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getTotalAhorro()
    {
        try {
            $user=$_SESSION['user']->ID_USUARIO;
            $strSql = "SELECT TRUNCATE(SUM(VALOR_PRODUCIDO),0) AS PRODUCIDO FROM pago_producido WHERE S_N_PRODUCIDO=2 AND S_N_CANCELADO=1";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getTotalAhorroById()
    {
        try {
            $user=$_SESSION['user']->ID_USUARIO;
            $strSql = "SELECT p.ID_CONVENIO AS CONVENIO,TRUNCATE(SUM(VALOR_PRODUCIDO),0) AS PRODUCIDO,a.S_N_CANCELADO,p.ID_PERSONA_FK AS PERSONA,u.ID_USUARIO AS USUARIO FROM convenio_producido p 
            INNER JOIN pago_producido a on a.ID_CONVENIO_FK=P.ID_CONVENIO
            INNER JOIN persona pe on pe.ID_PERSONA=p.ID_PERSONA_FK
            INNER JOIN usuario u on u.ID_USUARIO=pe.ID_USUARIO_FK
            WHERE S_N_PRODUCIDO=2 AND u.ID_USUARIO=$user AND a.S_N_CANCELADO=1 ;";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function newPayment($payment)
    {
        try {
           
            $data['S_N_CANCELADO'] =2 ;
            $data['ID_CONVENIO_FK']= $payment[0];
             $data['S_N_PRODUCIDO']=$payment[1];
              $data['VALOR_PRODUCIDO']=$payment[2];
               $data['FECHA_PRODUCIDO']=$payment[3];

            //$data['PASWORD']= hash('256', $data['PASWORD']);
            $this->pdo->insert('pago_producido', $data);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getPaymentById($id)
    {
        try {
            $strSql    = "SELECT * FROM pago_producido WHERE ID_PRODUCIDO = :id";
            $arrayData = ['id' => $id];
            $query     = $this->pdo->select($strSql, $arrayData);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function editPayment($data)
    {
        try {

            $strWhere = 'ID_PRODUCIDO = ' . $data["ID_PRODUCIDO"];
            $this->pdo->update('pago_producido', $data, $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function editPaymentStatus($data)
    {
        try {

            $strWhere = 'ID_PRODUCIDO = ' . $data["ID_PRODUCIDO"];
            $this->pdo->update('pago_producido', $data, $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function deletePayment($data)
    {
        try {
            var_dump($data);
            $strWhere = 'ID_PRODUCIDO = ' . $data['id'];
            $this->pdo->delete('pago_producido', $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
     public function getTotalEnero()
    {
        try {
             $strSql = "SELECT SUM(VALOR_PRODUCIDO) AS TOTAL FROM pago_producido WHERE FECHA_PRODUCIDO>='2020-01-01 00:00:00' AND FECHA_PRODUCIDO <= '2020-01-31 00:00:00'  AND S_N_CANCELADO=1";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getTotalFebrero()
    {
        try {
             $strSql = "SELECT SUM(VALOR_PRODUCIDO) AS TOTAL FROM pago_producido WHERE FECHA_PRODUCIDO>='2020-02-01 00:00:00' AND FECHA_PRODUCIDO <= '2020-02-31 00:00:00' AND S_N_CANCELADO=1";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getTotalMarzo()
    {
        try {
             $strSql = "SELECT SUM(VALOR_PRODUCIDO) AS TOTAL FROM pago_producido WHERE FECHA_PRODUCIDO>='2020-03-01 00:00:00' AND FECHA_PRODUCIDO <= '2020-03-31 00:00:00' AND S_N_CANCELADO=1";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getTotalAbril()
    {
        try {
             $strSql = "SELECT SUM(VALOR_PRODUCIDO) AS TOTAL FROM pago_producido WHERE FECHA_PRODUCIDO>='2020-04-01 00:00:00' AND FECHA_PRODUCIDO <= '2020-04-31 00:00:00' AND S_N_CANCELADO=1";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getTotalMayo()
    {
        try {
             $strSql = "SELECT SUM(VALOR_PRODUCIDO) AS TOTAL FROM pago_producido WHERE FECHA_PRODUCIDO>='2020-05-01 00:00:00' AND FECHA_PRODUCIDO <= '2020-05-31 00:00:00' AND S_N_CANCELADO=1";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
     public function getTotalJunio()
    {
        try {
             $strSql = "SELECT SUM(VALOR_PRODUCIDO) AS TOTAL FROM pago_producido WHERE FECHA_PRODUCIDO>='2020-06-01 00:00:00' AND FECHA_PRODUCIDO <= '2020-06-31 00:00:00' AND S_N_CANCELADO=1";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
     public function getTotalJulio()
    {
        try {
             $strSql = "SELECT SUM(VALOR_PRODUCIDO) AS TOTAL FROM pago_producido WHERE FECHA_PRODUCIDO>='2020-07-01 00:00:00' AND FECHA_PRODUCIDO <= '2020-07-31 00:00:00'  AND S_N_CANCELADO=1";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getTotalAgosto()
    {
        try {
             $strSql = "SELECT SUM(VALOR_PRODUCIDO) AS TOTAL FROM pago_producido WHERE FECHA_PRODUCIDO>='2020-08-01 00:00:00' AND FECHA_PRODUCIDO <= '2020-08-31 00:00:00'  AND S_N_CANCELADO=1";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getTotalSeptiembre()
    {
        try {
             $strSql = "SELECT SUM(VALOR_PRODUCIDO) AS TOTAL FROM pago_producido WHERE FECHA_PRODUCIDO>='2020-09-01 00:00:00' AND FECHA_PRODUCIDO <= '2020-09-31 00:00:00' AND S_N_CANCELADO=1";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getTotalOctubre()
    {
        try {
             $strSql = "SELECT TRUNCATE(SUM(VALOR_PRODUCIDO),0) AS TOTAL FROM pago_producido WHERE FECHA_PRODUCIDO>='2020-10-01 00:00:00' AND FECHA_PRODUCIDO <= '2020-10-31 00:00:00' AND S_N_CANCELADO=1";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getTotalNoviembre()
    {
        try {
             $strSql = "SELECT SUM(VALOR_PRODUCIDO) AS TOTAL FROM pago_producido WHERE FECHA_PRODUCIDO>='2020-11-01 00:00:00' AND FECHA_PRODUCIDO <= '2020-11-31 00:00:00' AND S_N_CANCELADO=1";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
     public function getTotalDiciembre()
    {
        try {
             $strSql = "SELECT SUM(VALOR_PRODUCIDO) AS TOTAL FROM pago_producido WHERE FECHA_PRODUCIDO>='2020-12-01 00:00:00' AND FECHA_PRODUCIDO <= '2020-12-31 00:00:00' AND S_N_CANCELADO=1";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

}
