<?php
	
	/**
	 * Modelo de la Tabla usuario
	 */
	class User
	{
		
		private $ID_PERSONA;
		private $DOCU_PERSONA;
		private $NOM_PERSONA;
		private $APE_PERSONA;
		private $TEL_PERSONA;
		private $CORREO_PERSONA;
		private $DIREC_PERSONA;
		private $ID_USUARIO_FK;
		private $ID_ROL_FK_P;

		private $ID_USUARIO;
		private $NOM_USUARIO;
		private $CONTRA_USUARIO;
		private $ID_ROL_FK_U;
		private $ID_ESTADO_FK;

		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT P.*,U.*,R.* FROM usuario U, persona P, rol R WHERE 
				P.ID_USUARIO_FK = U.ID_USUARIO AND P.ID_ROL_FK = R.ID_ROL AND U.ID_ROL_FK = R.ID_ROL";

				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function newUser($data)
		{
			try {
				

				$dataUser['NOM_USUARIO'] = $data['NOM_USUARIO'];
				$dataUser['CONTRA_USUARIO'] = $data['PASSWORD'];
				$dataUser['ID_ROL_FK'] = $data['ID_ROL_FK'];
				$dataUser['ID_ESTADO_FK'] = 1;

				$this->pdo->insert('usuario', $dataUser);	


				$dataPerson['DOCU_PERSONA'] = $data['DOCU_PERSONA'];
				$dataPerson['NOM_PERSONA'] = $data['NOM_PERSONA'];
				$dataPerson['APE_PERSONA'] = $data['APE_PERSONA'];
				$dataPerson['TEL_PERSONA'] = $data['TEL_PERSONA'];
				$dataPerson['CORREO_PERSONA'] = $data['CORREO_PERSONA'];
				$dataPerson['DIREC_PERSONA'] = $data['DIREC_PERSONA'];

				$usu = $this->getUserIdFk($dataUser['NOM_USUARIO'],$dataUser['ID_ROL_FK']);
				$dataPerson['ID_USUARIO_FK'] = $usu[0]->ID_USUARIO;
				
				$dataPerson['ID_ROL_FK'] = $data['ID_ROL_FK'];

				$this->pdo->insert('persona', $dataPerson);	
			
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function getUserById($ID_USUARIO)
		{
			try {
				$strSql = "SELECT * FROM usuario WHERE ID_USUARIO = :id";
				$arrayData = ['id' => $ID_USUARIO];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function getUserDataById($ID_USUARIO)
		{
			try {
				$strSql = "SELECT * FROM usuario U,persona P WHERE U.ID_USUARIO=P.ID_USUARIO_FK AND U.ID_USUARIO= :id";
				$arrayData = ['id' => $ID_USUARIO];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}


		public function getUserIdFk($NOM_USUARIO, $ID_ROL_FK)
		{
			try {
				$strSql = "SELECT ID_USUARIO FROM usuario WHERE NOM_USUARIO = :nomUsu AND ID_ROL_FK = :idRol";
				$arrayData = ['nomUsu' => $NOM_USUARIO, 'idRol' => $ID_ROL_FK];
				$query = $this->pdo->select2($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function editUser($data)
		{
			try {

				$strWhereUsuario = 'ID_USUARIO = '. $data['ID_USUARIO'];
				$this->pdo->update('usuario', $data, $strWhereUsuario);	

			} catch(PDOException $e) {	
				die($e->getMessage());
			}		
		}

		public function editUserPerson($data)
		{
			try {

				$strWhereUsuario = 'ID_USUARIO = '. $data['ID_USUARIO'];
				$dataUser['NOM_USUARIO'] = $data['NOM_USUARIO'];
				$dataUser['CONTRA_USUARIO'] = $data['CONTRA_USUARIO'];
				$dataUser['ID_ROL_FK'] = $data['ID_ROL_FK'];
				
				$this->pdo->update('usuario', $dataUser, $strWhereUsuario);	


				$strWherePersona = 'ID_USUARIO_FK = '. $data['ID_USUARIO'];
				$dataPerson['NOM_PERSONA'] = $data['NOM_PERSONA'];
				$dataPerson['APE_PERSONA'] = $data['APE_PERSONA'];
				$dataPerson['TEL_PERSONA'] = $data['TEL_PERSONA'];
				$dataPerson['CORREO_PERSONA'] = $data['CORREO_PERSONA'];
				$dataPerson['DIREC_PERSONA'] = $data['DIREC_PERSONA'];
				$dataPerson['ID_ROL_FK'] = $data['ID_ROL_FK'];

				$this->pdo->update('persona', $dataPerson, $strWherePersona);	

			} catch(PDOException $e) {	
				die($e->getMessage());
			}		
		}

		public function deleteUser($data)
		{
			try {
				$strWhere = 'ID_USUARIO = '. $data['id'];
				$this->pdo->delete('usuario', $strWhere);
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function newpass($user,$newpass)
		{
			try {
				$strWhere = 'ID_USUARIO = ' . $user;
				$data['CONTRA_USUARIO']=$newpass;
            	$this->pdo->update('usuario', $data, $strWhere);
								
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function getUsuPass($user)
		{
			try {
				$strSql = "SELECT CONTRA_USUARIO FROM usuario  WHERE ID_USUARIO=$user";
				$query = $this->pdo->select($strSql);
				return $query[0]->CONTRA_USUARIO; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		/*
			Query para obtener los convenios: vehiculo y conductor
			que tiene un socio

			SELECT 
				V.ID_VEHICULO,
				V.PLACA_VEHICULO,
				R.ID_PERSONA AS ID_SOCIO,
				R.DOCU_PERSONA AS DOC_SOCIO,
				CONCAT(R.NOM_PERSONA," ",R.APE_PERSONA) AS NOM_SOCIO,
				C.*,
				S.ID_PERSONA AS ID_COND,
				S.DOCU_PERSONA AS DOC_COND,
				CONCAT(S.NOM_PERSONA," ",S.APE_PERSONA) AS NOM_COND 
			FROM 
				VEHICULO V, 
				PERSONA R, 
				USUARIO U,
				CONVENIO_PRODUCIDO C,
				PERSONA S 
			WHERE 
				V.ID_PERSONA_FK = R.ID_PERSONA AND 
				C.PLACA_VEHICULO_FK = V.PLACA_VEHICULO AND 
				C.ID_PERSONA_FK = S.ID_PERSONA AND 
				R.ID_USUARIO_FK = U.ID_USUARIO AND
				U.ID_USUARIO = ?;
		*/
		public function getAssignedDriversByUserId($ID_USUARIO)
		{
			try {
				$strSql = "
				SELECT
					C.ID_CONVENIO,
					S.ID_PERSONA,
					CONCAT(S.NOM_PERSONA,' ',S.APE_PERSONA) AS NOM_COND 
				FROM 
					vehiculo V, 
					persona R, 
					usuario U,
					convenio_producido C,
					persona S 
				WHERE 
					V.ID_PERSONA_FK = R.ID_PERSONA AND 
					C.PLACA_VEHICULO_FK = V.PLACA_VEHICULO AND 
					C.ID_PERSONA_FK = S.ID_PERSONA AND 
					R.ID_USUARIO_FK = U.ID_USUARIO AND
					U.ID_USUARIO = :id";
				$arrayData = ['id' => $ID_USUARIO];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

	}